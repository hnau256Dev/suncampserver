package ru.hnau.suncamp.server.api.rest

import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType


object RequestByIpValidator {

    const val LIGHT_WEIGHT = 1
    const val MEDIUM_WEIGHT = 2
    const val HARD_WEIGHT = 10

    private const val MINUS_PER_SECOND = 1L
    private const val CRITICAL_SUM = 100L
    private const val MAX_SUM = CRITICAL_SUM * 2

    private const val REQUEST_COUNT_TO_CLEAR = 1000

    private var ipSumMap: MutableMap<String, Pair<Long, Long>> = HashMap()

    private var requestsCountForClear = 0

    fun validate(ip: String?, requestWeight: Long) {
        incRequestsCountAndClearIfNeed()
        if (ip == null) {
            throw ApiErrorException(ApiErrorType.IP_BLOCKED)
        }

        val lastWeight = ipSumMap[ip]?.let { (time, weight) ->
            getWeightForIp(time, weight)
        } ?: 0L

        val newWeight = lastWeight + requestWeight
        ipSumMap[ip] = System.currentTimeMillis() to newWeight

        if (newWeight >= CRITICAL_SUM) {
            throw ApiErrorException(
                    type = ApiErrorType.IP_BLOCKED,
                    params = mapOf("seconds" to (newWeight - CRITICAL_SUM).toString())
            )
        }

    }

    private fun getWeightForIp(time: Long, weight: Long) =
            (weight - (System.currentTimeMillis() - time) / 1000 * MINUS_PER_SECOND)
                    .coerceIn(0L, MAX_SUM)

    private fun incRequestsCountAndClearIfNeed() = synchronized(this) {
        requestsCountForClear++
        if (requestsCountForClear < REQUEST_COUNT_TO_CLEAR) {
            return@synchronized
        }

        requestsCountForClear = 0

        ipSumMap = ipSumMap.filterValues { (time, weight) ->
            getWeightForIp(time, weight) > 0
        }.toMutableMap()
    }

}