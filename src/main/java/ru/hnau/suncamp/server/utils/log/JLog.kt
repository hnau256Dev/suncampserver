package ru.hnau.suncamp.server.utils.log


interface JLog {

    fun i(msg: String, th: Throwable? = null)

    fun t(msg: String, th: Throwable? = null)

    fun d(msg: String, th: Throwable? = null)

    fun w(msg: String, th: Throwable? = null)

    fun e(msg: String, th: Throwable? = null)

}