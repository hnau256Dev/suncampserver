package ru.hnau.suncamp.server.utils.log


class JMethodLog(
        sessionLog: JSessionLog,
        private val className: String,
        private val methodName: String
) : JLogWrapper(sessionLog) {

    constructor(
            sessionLog: JSessionLog,
            clazz: Class<*>,
            methodName: String
    ) : this(
            sessionLog,
            clazz.simpleName,
            methodName
    )

    override fun formatMessage(msg: String) = "[$className:$methodName] $msg"

}