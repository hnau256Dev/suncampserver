package ru.hnau.suncamp.server.data.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.hnau.suncamp.server.data.db.UserDB


interface UserRepository : MongoRepository<UserDB, String>