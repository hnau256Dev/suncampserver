package ru.hnau.suncamp.server.data.db

import com.google.gson.Gson
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.suncamp.common.data.StorageManager


@Document(collection = "storage_manager")
data class StorageManagerDB(
        @Id val key: String? = null
) {

    companion object {

        private val GSON = Gson()

    }

    constructor(storageManager: StorageManager) : this(GSON.toJson(storageManager))

    constructor(masterUserLogin: String, slaveUserLogin: String) : this(
            StorageManager(
                    masterUserLogin = masterUserLogin,
                    slaveUserLogin = slaveUserLogin
            )
    )

    val storageManager: StorageManager
        get() = GSON.fromJson(key, StorageManager::class.java)

}