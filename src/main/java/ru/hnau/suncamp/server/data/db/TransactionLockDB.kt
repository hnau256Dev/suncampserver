package ru.hnau.suncamp.server.data.db

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document


@Document(collection = "transaction_lock")
data class TransactionLockDB(
        @Id val key: String
)