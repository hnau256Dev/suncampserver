package ru.hnau.suncamp.server.data.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.hnau.suncamp.server.data.db.TransactionLockDB


interface TransactionLockRepository : MongoRepository<TransactionLockDB, String>