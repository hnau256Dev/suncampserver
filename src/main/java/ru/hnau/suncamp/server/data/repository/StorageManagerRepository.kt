package ru.hnau.suncamp.server.data.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.hnau.suncamp.server.data.db.CardDB
import ru.hnau.suncamp.server.data.db.StorageManagerDB


interface StorageManagerRepository : MongoRepository<StorageManagerDB, String>