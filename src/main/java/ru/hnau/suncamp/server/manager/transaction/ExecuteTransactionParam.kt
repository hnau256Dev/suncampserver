package ru.hnau.suncamp.server.manager.transaction

import ru.hnau.suncamp.common.data.TransactionCategory
import ru.hnau.suncamp.server.utils.log.JSessionLog


data class ExecuteTransactionParam(
        val authToken: String,
        val fromUserLogin: String,
        val toUserLogin: String,
        val amount: Long,
        val category: TransactionCategory,
        val comment: String,
        val associatedTransactionId: Long?
) {

    companion object {

        private const val LOGINS_SEPARATOR = ","

    }

    val fromUsersLogins: List<String>
        get() = fromUserLogin.split(LOGINS_SEPARATOR)

    val toUsersLogins: List<String>
        get() = toUserLogin.split(LOGINS_SEPARATOR)

}