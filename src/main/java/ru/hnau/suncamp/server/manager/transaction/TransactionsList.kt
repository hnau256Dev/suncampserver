package ru.hnau.suncamp.server.manager.transaction

import ru.hnau.suncamp.common.data.Transaction


data class TransactionsList(
        val positiveTransactions: List<Transaction>,
        val negativeTransactions: List<Transaction>
)