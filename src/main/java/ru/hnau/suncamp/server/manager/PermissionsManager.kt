package ru.hnau.suncamp.server.manager

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncamp.server.data.db.UserDB


@Component
class PermissionsManager {

    @Autowired
    lateinit var storageManagerManager: StorageManagerManager

    fun allowUserManageUser(master: UserDB, slave: UserDB): Boolean {
        //Сам собой
        if (master.login == slave.login) {
            return true
        }

        //Управляющий пользователем
        val allowManageUsersAccounts = master.type?.allowManageUsersAccounts == true
        if (allowManageUsersAccounts && slave.type == UserType.user) {
            return true
        }

        //Управляющий хранилищем
        val allowManageStoragesAccounts = master.type?.allowManageStoragesAccounts == true
        if (
                allowManageStoragesAccounts &&
                slave.type == UserType.storage &&
                storageManagerManager.checkIsAllowUserToManageStorage(
                        masterLogin = master.login!!,
                        slaveLogin = slave.login!!
                )
        ) {
            return true
        }

        return false
    }

}