package ru.hnau.suncamp.server.manager.transaction

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncamp.server.manager.PermissionsManager
import ru.hnau.suncamp.server.manager.UserManager
import ru.hnau.suncamp.server.utils.log.JSessionLog
import ru.hnau.suncamp.server.utils.sumByLong


@Component
class BalanceManager {

    @Autowired
    lateinit var transactionManager: TransactionManager

    @Autowired
    lateinit var permissionsManager: PermissionsManager

    @Autowired
    lateinit var userManager: UserManager

    fun invalidateAllUserBalances(sessionLog: JSessionLog) {
        UserBalanceCache.clear()
        val logger = sessionLog.getMethodLog(BalanceManager::class.java, "invalidateAllUserBalances")
        logger.d("All users balances invalidated")
    }

    fun getBalanceByLoginInner(login: String, sessionLog: JSessionLog): Long {
        val logger = sessionLog.getMethodLog(BalanceManager::class.java, "getBalanceByLoginInner")

        val cachedBalance = UserBalanceCache.get(login)
        if (cachedBalance != null) {
            logger.d("No need calculate balance for user ($login), there is balance in cache: $cachedBalance")
            return cachedBalance
        }

        logger.d("Calculating balance for user ($login) by his transactions")
        val transactions = transactionManager.getTransactionsInner(login)
        val transactionsAmount = transactions.positiveTransactions.sumByLong { it.amount } - transactions.negativeTransactions.sumByLong { it.amount }
        val baseAmount = userManager.findUserByLogin(login, sessionLog).baseAmount!!
        val userBalance = baseAmount + transactionsAmount

        logger.d("Saving user ($login) balance ($userBalance) to cache")
        UserBalanceCache.set(login, userBalance)

        return userBalance
    }

    fun getBalance(authToken: String, login: String, sessionLog: JSessionLog): Long {
        val master = userManager.findUserByAuthToken(authToken, sessionLog)
        val slave = userManager.findUserByLogin(login, sessionLog)
        val hasPermissions = permissionsManager.allowUserManageUser(master, slave)
        if (!hasPermissions) {
            throw ApiErrorException(ApiErrorType.PERMISSION_DENIED)
        }
        return getBalanceByLoginInner(login, sessionLog)
    }

    fun getBalancesOfUsers(authToken: String, sessionLog: JSessionLog): List<Pair<String, Long>> {

        val user = userManager.findUserByAuthToken(authToken, sessionLog)
        if (user.type?.allowManageUsersAccounts != true) {
            throw ApiErrorException(ApiErrorType.PERMISSION_DENIED)
        }

        return userManager.getAll().mapNotNull {
            if (it.type != UserType.user) {
                return@mapNotNull null
            }
            val userLogin = it.login
            val balance = getBalanceByLoginInner(userLogin, sessionLog)
            userLogin to balance
        }
    }

    fun getBalanceStatistic(sessionLog: JSessionLog): List<Pair<String, Any>> {
        val usersBalances = userManager
                .getAll()
                .filter { it.type == UserType.user }
                .map {
                    val login = it.login
                    val name = it.name
                    val balance = getBalanceByLoginInner(login, sessionLog)
                    name to balance
                }
                .sortedByDescending {
                    it.second
                }
        val sum = usersBalances.sumByLong { it.second }
        val count = usersBalances.size
        val average = if (count <= 0) 0f else (sum.toFloat() / count.toFloat())
        return usersBalances + ("Сумма" to sum) + ("Среднее" to average)
    }

}