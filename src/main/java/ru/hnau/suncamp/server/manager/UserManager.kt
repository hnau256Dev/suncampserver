package ru.hnau.suncamp.server.manager

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Component
import ru.hnau.jutils.TimeValue
import ru.hnau.suncamp.common.api.error.ApiErrorException
import ru.hnau.suncamp.common.api.error.ApiErrorType
import ru.hnau.suncamp.common.data.LoginResult
import ru.hnau.suncamp.common.data.UserType
import ru.hnau.suncamp.common.validators.LoginValidator
import ru.hnau.suncamp.common.validators.PasswordValidator
import ru.hnau.suncamp.common.validators.UserNameValidator
import ru.hnau.suncamp.server.data.db.UserDB
import ru.hnau.suncamp.server.data.repository.UserRepository
import ru.hnau.suncamp.server.utils.genUUID
import ru.hnau.suncamp.server.utils.log.JSessionLog
import ru.hnau.suncamp.server.utils.toHash
import java.util.NoSuchElementException

@Component
class UserManager {

    companion object {

        private val AUTH_TOKEN_LIFETIME = (TimeValue.MINUTE * 10).milliseconds

    }

    @Autowired
    lateinit var userRepository: UserRepository

    fun login(login: String, password: String, sessionLog: JSessionLog): LoginResult {

        val logger = sessionLog.getMethodLog(UserManager::class.java, "login")

        val user = try {
            userRepository.findOne(Example.of(UserDB(login = login, passwordHash = password.toHash()))).get()
        } catch (ex: NoSuchElementException) {
            logger.w("No user found for login '$login' and password '*****'")
            throw ApiErrorException(ApiErrorType.INCORRECT_LOGIN_OR_PASSWORD)
        }

        logger.i("User found: $user")

        val authToken = genUUID()
        val loggedUser = user.copy(
                authToken = authToken,
                lastLogged = System.currentTimeMillis()
        )
        userRepository.save(loggedUser)

        logger.i("Updated user authToken and lastLogged")

        return LoginResult(
                authToken = authToken,
                user = loggedUser.user
        )
    }

    fun findUserByAuthToken(authToken: String, sessionLog: JSessionLog): UserDB {

        val logger = sessionLog.getMethodLog(UserManager::class.java, "findUserByAuthToken")

        if (authToken.isBlank()) {
            logger.w("AuthToken is blank")
            throw ApiErrorException(ApiErrorType.NEED_LOGIN)
        }

        val user = try {
            userRepository.findOne(Example.of(UserDB(authToken = authToken))).get()
        } catch (ex: NoSuchElementException) {
            logger.w("No user found for authToken")
            throw ApiErrorException(ApiErrorType.NEED_LOGIN)
        }

        if (user.lastLogged == null || System.currentTimeMillis() > user.lastLogged + AUTH_TOKEN_LIFETIME) {
            logger.w("Auth token is outdated, need login")
            throw ApiErrorException(ApiErrorType.NEED_LOGIN)
        }

        return user
    }

    fun findUserByLogin(login: String, sessionLog: JSessionLog): UserDB {

        val logger = sessionLog.getMethodLog(UserManager::class.java, "findUserByLogin")

        if (login.isBlank()) {
            logger.w("Login is blank")
            throw ApiErrorException(ApiErrorType.USER_NOT_FOUND)
        }

        return try {
            userRepository.findOne(Example.of(UserDB(login = login))).get()
        } catch (ex: NoSuchElementException) {
            logger.w("No user found by login: '$login'")
            throw ApiErrorException(ApiErrorType.USER_NOT_FOUND)
        }

    }

    fun register(login: String, password: String, name: String, sessionLog: JSessionLog) {

        val logger = sessionLog.getMethodLog(UserManager::class.java, "register")

        LoginValidator.validateLogin(login)

        try {
            userRepository.findOne(Example.of(UserDB(login = login))).get()
            logger.w("User with login '$login' already exists")
            throw ApiErrorException(ApiErrorType.LOGIN_ALREADY_REGISTERED)
        } catch (ex: NoSuchElementException) {
        }

        PasswordValidator.validatePassword(password)
        UserNameValidator.validateUserName(name)

        val user = UserDB(
                login = login,
                passwordHash = password.toHash(),
                name = name,
                type = UserType.user,
                authToken = "",
                lastLogged = 0,
                baseAmount = 0
        )

        userRepository.save(user)
        logger.d("User $user registered")
    }

    fun updatePassword(authToken: String, newPassword: String, sessionLog: JSessionLog) {

        val user = findUserByAuthToken(authToken, sessionLog)

        PasswordValidator.validatePassword(newPassword)

        val logger = sessionLog.getMethodLog(UserManager::class.java, "updatePassword")

        val newUser = user.copy(
                passwordHash = newPassword.toHash()
        )
        userRepository.save(newUser)

        logger.d("Password for user '$user' updated")
    }

    fun updateName(authToken: String, newName: String, sessionLog: JSessionLog) {

        val user = findUserByAuthToken(authToken, sessionLog)

        UserNameValidator.validateUserName(newName)

        val logger = sessionLog.getMethodLog(UserManager::class.java, "updateName")

        val newUser = user.copy(
                name = newName
        )
        userRepository.save(newUser)

        logger.d("Name for user '$user' updated")
    }

    fun getAll() = userRepository.findAll().map { it.user }

}